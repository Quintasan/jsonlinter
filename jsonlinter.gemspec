# frozen_string_literal: true

require_relative "lib/jsonlinter/version"

Gem::Specification.new do |spec|
  spec.name = "jsonlinter"
  spec.version = Jsonlinter::VERSION
  spec.authors = ["Michał Zając"]
  spec.email = ["mzajac@gitlab.com"]

  spec.summary = "Yet another JSON linting utility"
  spec.description = "You can use this gem to lint your JSON files"
  spec.homepage = "https://gitlab.com/gitlab-org/jsonlinter"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 3.0"

  spec.metadata["allowed_push_host"] = "https://rubygems.org"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/gitlab-org/jsonlinter/-/tree/master"
  spec.metadata["changelog_uri"] = "https://gitlab.com/gitlab-org/jsonlinter/-/blob/master/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      files_to_exclude = %w[
        .standard.yml
        sig/
        bin/
        test/
        spec/
        features/
        .git
        .circleci
        appveyor
        rbs_collection.lock.yaml
        rbs_collection.yaml
        Rakefile
        Steepfile
      ]
      (File.expand_path(f) == __FILE__) || f.start_with?(*files_to_exclude)
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "concurrent-ruby", "~> 1.2"
  spec.add_dependency "fast_ignore", "~> 0.17"
  spec.add_dependency "tty-logger", "~> 0.6"
  spec.add_dependency "tty-option", "~> 0.3"
  spec.metadata["rubygems_mfa_required"] = "true"
end
