# frozen_string_literal: true

require "test_helper"

class TestJsonlinterFileScanner < Minitest::Test
  def setup
    @fixtures_path = Pathname.new(Jsonlinter.root.join("test", "fixtures"))
    @fixture_file = Pathname.new(@fixtures_path).join("example.json")
    @all_fixtures = Dir[@fixtures_path.join("**", "*.json").to_s].map { |p| Pathname.new(p) }
  end

  class TestInitialize < TestJsonlinterFileScanner
    def test_it_can_be_initialized_with_pathname
      file_scanner = Jsonlinter::FileScanner.new(@fixtures_path)

      refute_nil file_scanner
    end

    def test_it_can_be_initialized_with_string
      file_scanner = Jsonlinter::FileScanner.new(@fixtures_path.to_s)

      refute_nil file_scanner
    end

    def test_it_cant_be_initialized_with_anything_else
      assert_raises(StandardError) { Jsonlinter::FileScanner.new(1) }
    end
  end

  class TestAllJsonFiles < TestJsonlinterFileScanner
    def test_when_given_a_file
      file_scanner = Jsonlinter::FileScanner.new(@fixture_file)

      assert_equal file_scanner.all_json_files, [@fixture_file.expand_path]
    end

    def test_when_given_a_directory
      file_scanner = Jsonlinter::FileScanner.new(@fixtures_path)

      assert_equal file_scanner.all_json_files, @all_fixtures
    end
  end

  class TestFdUnavailable < TestJsonlinterFileScanner
    def test_when_given_a_file
      file_scanner = Jsonlinter::FileScanner.new(@fixture_file)

      file_scanner.stub(:fd_available?, false) do
        assert_equal file_scanner.all_json_files, [@fixture_file.expand_path]
      end
    end

    def test_when_given_a_directory
      file_scanner = Jsonlinter::FileScanner.new(@fixtures_path)

      file_scanner.stub(:fd_available?, false) do
        assert_equal file_scanner.all_json_files, @all_fixtures
      end
    end
  end

  class TestExcludedFiles < TestJsonlinterFileScanner
    def test_when_excludefile_exists
      file_scanner = Jsonlinter::FileScanner.new(@fixtures_path)

      example_excluded_files = %w[example.json]

      Jsonlinter::ExcludeFileReader.stub(:read, example_excluded_files) do
        assert_equal file_scanner.excluded_files, example_excluded_files
      end
    end

    def test_when_excludefile_does_not_exist
      file_scanner = Jsonlinter::FileScanner.new(@fixtures_path)

      assert_empty file_scanner.excluded_files
    end
  end

  class TestAllMatchingFiles < TestJsonlinterFileScanner
    def test_when_excludefile_exists
      file_scanner = Jsonlinter::FileScanner.new(@fixtures_path)
      example_excluded_files = %w[example.json]

      Jsonlinter::ExcludeFileReader.stub(:read, example_excluded_files) do
        assert_equal file_scanner.all_matching_files, @all_fixtures - example_excluded_files
      end
    end

    def test_when_excludefile_does_not_exist
      file_scanner = Jsonlinter::FileScanner.new(@fixtures_path)

      assert_equal file_scanner.all_matching_files, @all_fixtures
    end
  end
end
