# frozen_string_literal: true

require "test_helper"

class TestJsonlinter < Minitest::Test
  def test_that_version_matches_git_tag
    latest_git_tag = `git tag --list | sort | tail -1`.chomp

    assert_equal latest_git_tag[1..], ::Jsonlinter::VERSION
  end
end
