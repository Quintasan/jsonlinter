# frozen_string_literal: true

require_relative "jsonlinter/cli"
require_relative "jsonlinter/version"
require_relative "jsonlinter/file_scanner"
require_relative "jsonlinter/discriminator"
require_relative "jsonlinter/pretty_writer"
require_relative "jsonlinter/logger"

module Jsonlinter
  def self.root
    Pathname.new(File.dirname(__dir__))
  end
end
