# frozen_string_literal: true

require_relative "refinements/split_array_into_n_parts"

using Jsonlinter::Refinements::SplitArrayIntoNParts

module Jsonlinter
  class PrettyWriter
    def initialize(files_to_write)
      @files_to_write = files_to_write
    end

    def call
      jobs = @files_to_write.in_groups(4).map do |group|
        Concurrent::Promises.future { pretty_file(group) }
      end

      @results = Concurrent::Promises.zip(*jobs).value.reduce({}, :merge)

      [select_by_state(:success), select_by_state(:failure)]
    end

    def select_by_state(state)
      @results.select { |_filename, result| result.first == state }
    end

    def pretty_file(files)
      files = Array(files)
      results = {}
      files.each do |file|
        json = File.read(file)
        pretty = JSON.pretty_generate(JSON.parse(json)) << "\n"

        File.write(file, pretty)
        results[file] = [:success, ""]
      rescue StandardError => e
        results[file] = [:failure, e.message]
      end
      results
    end
  end
end
