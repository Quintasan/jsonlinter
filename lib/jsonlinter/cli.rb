# frozen_string_literal: true

require "tty-option"
require_relative "runner"

module Jsonlinter
  class Cli
    include TTY::Option

    usage do
      program "jsonlinter"

      command "run"
    end

    argument :paths do
      name "path"
      arity one_or_more
      convert :pathname_list
      desc "Paths (or files) to scan"
    end

    flag :list_files do
      short "-l"
      long "--list-files"
      desc "Print list of files that require linting, exit code will be 1 if any files require linting"
    end

    flag :autocorrect do
      short "-a"
      long "--autocorrect"
      desc "Automatically prettify JSON files"
    end

    flag :help do
      short "-h"
      long "--help"
      desc "Print usage"
    end

    flag :silent do
      long "--silent"
      desc "Do not display any output. Combine with --list-files if you want to ONLY list the files " \
           "that require linting"
    end

    def run
      if params[:help]
        print help
      elsif params.errors.any?
        puts params.errors.summary
      else
        Jsonlinter::Runner.call(params)
      end
    end
  end
end
