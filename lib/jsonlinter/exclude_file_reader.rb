# frozen_string_literal: true

module Jsonlinter
  class ExcludeFileReader
    def self.read(path = nil)
      path = File.join(Dir.getwd, ".jsonlinter_exclude") if path.nil?
      File.read(path).split("\n").map { |exclude_path| Pathname.new(exclude_path).expand_path }
    rescue Errno::ENOENT
      []
    end
  end
end
