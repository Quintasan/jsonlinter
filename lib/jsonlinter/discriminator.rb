# frozen_string_literal: true

require "concurrent"
require "json"

require_relative "refinements/split_array_into_n_parts"

using Jsonlinter::Refinements::SplitArrayIntoNParts

module Jsonlinter
  class Discriminator
    def initialize(files_to_scan)
      @files_to_scan = files_to_scan
      @results = nil
    end

    def call
      jobs = @files_to_scan.in_groups(4).map do |group|
        Concurrent::Promises.future { scan_files(group) }
      end

      @results = Concurrent::Promises.zip(*jobs).value.reduce({}, :merge)

      [select_by_state(:pretty), select_by_state(:todo), select_by_state(:invalid)]
    end

    def select_by_state(state)
      @results.select { |_filename, file_state| file_state == state }
    end

    def scan_files(files)
      files = Array(files)
      results = {}
      files.each do |file|
        json = File.read(file.to_s)
        pretty = JSON.pretty_generate(JSON.parse(json)) << "\n"

        results[file] = if json == pretty
                          :pretty
                        else
                          :todo
                        end
      rescue JSON::ParserError
        results[file] = :invalid
      end
      results
    end
  end
end
