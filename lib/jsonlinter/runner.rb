# frozen_string_literal: true

require "tty-logger"
require_relative "file_scanner"
require_relative "discriminator"
require_relative "pretty_writer"
require_relative "logger"

module Jsonlinter
  class Runner
    include Jsonlinter::Logger

    def initialize(params)
      @params = params
    end

    def self.call(params)
      new(params).call
    end

    def call
      matching_files = scan_files(@params[:paths])

      exit_if_no_files(matching_files)

      _pretty, todo, _invalid = group_files(matching_files)

      if !@params[:autocorrect] && @params[:list_files]
        if todo.any?
          list_invalid_files(todo)
          exit(1)
        else
          exit(0)
        end
      end

      prettify_files(todo) if @params[:autocorrect]
    end

    private

    def exit_if_no_files(matches)
      return unless matches.empty?

      logger.info { "No files to scan, exitting early" }
      exit(0)
    end

    def list_invalid_files(paths)
      logger.info { "The following files require linting:" }
      filenames = paths.keys

      logger.add_handler(:console) if @params[:silent] && @params[:list_files]
      filenames.each do |filename|
        logger.plain { filename.to_s }
      end
      logger.remove_handler(:console) if @params[:silent] && @params[:list_files]
    end

    def prettify_files(paths)
      prettied, failed = Jsonlinter::PrettyWriter.new(paths.keys).call

      logger.success { "Prettied #{prettied.keys.count} files" }
      logger.error { "Failed to pretty print #{failed.keys.count} files" } if failed.keys.any?
    end

    def scan_files(paths)
      logger.info { "Scanning #{paths.count} paths..." }
      scanner = Jsonlinter::FileScanner.new(paths)

      all_files = scanner.all_json_files
      excluded_files = scanner.excluded_files
      matching_files = scanner.all_matching_files

      logger.info { "Found #{all_files.count} JSON files" }
      logger.info { "Excluded #{excluded_files.count} JSON files" }
      logger.info { "#{matching_files.count} files will be scanned" }

      matching_files
    end

    def group_files(paths)
      discriminator = Jsonlinter::Discriminator.new(paths)
      pretty, todo, invalid = discriminator.call

      logger.pretty { "#{pretty.keys.count} JSON files" }
      logger.unpretty { "#{todo.keys.count} JSON files" }
      logger.invalid { "#{invalid.keys.count} invalid JSON files" }

      [pretty, todo, invalid]
    end
  end
end
