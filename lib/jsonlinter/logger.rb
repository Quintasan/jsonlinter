# frozen_string_literal: true

require "tty-logger"

module Jsonlinter
  module Logger
    def logger(silent: false)
      @logger ||= if silent
                    silent_logger
                  else
                    pretty_logger
                  end
    end

    private

    def silent_logger
      TTY::Logger.new do |config|
        config.level = ENV.key?("JSONLINTER_DEBUG") ? :debug : :info
        config.types = {
          pretty: { level: :info },
          unpretty: { level: :info },
          invalid: { level: :info },
          plain: { level: :info }
        }
        config.handlers = [:null]
      end
    end

    def pretty_logger
      TTY::Logger.new do |config| # rubocop:disable Metrics/BlockLength
        config.level = ENV.key?("JSONLINTER_DEBUG") ? :debug : :info
        config.types = {
          pretty: { level: :info },
          unpretty: { level: :info },
          invalid: { level: :info },
          plain: { level: :info }
        }
        config.handlers = [
          [:console, {
            styles: {
              pretty: {
                symbol: "✔ ",
                label: "pretty",
                color: :green,
                levelpad: 2
              },
              unpretty: {
                symbol: "⚠ ",
                label: "unpretty",
                color: :yellow,
                levelpad: 0
              },
              invalid: {
                symbol: "⨯",
                label: "invalid",
                color: :red,
                levelpad: 2
              },
              plain: {
                symbol: "",
                label: "",
                color: :white,
                levelpad: 0
              }
            }
          }]
        ]
      end
    end
  end
end
