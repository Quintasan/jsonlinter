# frozen_string_literal: true

module Jsonlinter
  module Refinements
    module SplitArrayIntoNParts
      refine Array do
        def in_groups(number_of_groups)
          slice_size = (size / number_of_groups.to_f).round

          return to_a if slice_size <= 0

          each_slice(slice_size).to_a
        end
      end
    end
  end
end
