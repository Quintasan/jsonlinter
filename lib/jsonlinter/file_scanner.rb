# frozen_string_literal: true

require "pathname"
require "pry-byebug"
require_relative "exclude_file_reader"
require_relative "logger"

module Jsonlinter
  class FileScanner
    include Jsonlinter::Logger

    def initialize(paths)
      @paths = Array(paths).map do |path|
        if path.is_a?(::Pathname)
          path
        else
          ::Pathname.new(File.expand_path(path))
        end
      end
    end

    def excluded_files
      @excluded_files ||= Jsonlinter::ExcludeFileReader.read
    end

    def all_json_files
      @paths.flat_map do |path|
        if path.directory?
          find_all_json_files(path)
        else
          [path.expand_path]
        end
      end
    end

    def all_matching_files
      all_json_files - excluded_files
    end

    private

    def find_all_json_files(path)
      return find_using_fd(path) if fd_available?

      find_using_fast_ignore
    end

    def find_using_fd(path)
      logger.debug { "Using fd" }
      IO
        .popen("fd . -t file -e json #{path.expand_path}")
        .map(&:chomp)
        .map { |filepath| Pathname.new(filepath).expand_path }
    end

    def fd_available?
      `fd --version`.chomp =~ /fd %d\.%d\.%d/
      `fd --version`.chomp.match?(/^fd\ (\d)+\.(\d)+\.(\d)+$/)
    rescue Errno::ENOENT
      false
    end

    def find_using_fast_ignore
      logger.debug { "Using FastIgnore" }
      require "fast_ignore"
      FastIgnore
        .new
        .select { |path| path.end_with?(".json") }
        .map { |path| Pathname.new(path).expand_path }
    end
  end
end
